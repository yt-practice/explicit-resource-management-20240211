import 'disposablestack/auto'

const sub1 = () => {
  console.log('start')
  using _ = {
    [Symbol.dispose]: () => {
      console.log('close')
    },
  }
  console.log('end')
}

class HogeError extends Error {}

const sub2 = () => {
  console.log('start')
  using _1 = {
    [Symbol.dispose]: () => {
      throw new HogeError()
    },
  }
  using _2 = {
    [Symbol.dispose]: () => {
      throw new HogeError()
    },
  }
  console.log('end')
}

const isSuppressedError = (x: unknown): x is SuppressedError =>
  x instanceof Error && 'SuppressedError' === x.name

export const main = () => {
  try {
    sub1()
    sub2()
  } catch (error) {
    if (isSuppressedError(error)) {
      console.log('error', error instanceof HogeError)
      console.log('error.error', error.error instanceof HogeError)
      console.log('error.suppressed', error.suppressed instanceof HogeError)
    } else {
      console.log('unknown', error)
    }
  }
}

main()
